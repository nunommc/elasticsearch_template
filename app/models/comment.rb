class Comment < ActiveRecord::Base
  belongs_to :article, touch: true

  # attr_accessible :body, :pick, :stars, :user, :user_location
end
