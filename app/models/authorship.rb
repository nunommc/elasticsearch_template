class Authorship < ActiveRecord::Base
  belongs_to :article, touch: true

  belongs_to :author
  # attr_accessible :title, :body
end
